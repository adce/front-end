import { Injectable } from "@angular/core"
import { Observable } from "rxjs"

import { environment } from "src/environments/environment"
import { VisitorData } from 'src/app/page/visitors/model/visitor.model'
import { isTerminating } from "apollo-link/lib/linkUtils"
import { subscribe } from "graphql"

@Injectable()
export class VisitorListening {
    private eventSource: EventSource

    constructor() {
        console.info('Conecting on visitor sse')
        this.eventSource = new EventSource(`${environment.SERVER}/sse`)
        this.eventSource.onopen = () => {
            console.log(`Conectado no SSE: ${this.eventSource.readyState}`)
        }
        this.eventSource.onerror = () => console.error(`Ocorreu um erro: ${this.eventSource.readyState}`)
    }

    onAddVisitor(): Observable<VisitorData> {
        return new Observable<VisitorData>(subscribe => {
            this.eventSource.addEventListener('addVisitor', event => {
                if(event instanceof MessageEvent) {
                    event as MessageEvent
                    const response = JSON.parse(event.data)
                    const visitor = response.data
                    subscribe.next(visitor)
                }
            })
        })
    }

    onRemoveVisitor(): Observable<VisitorData> {
        return new Observable<VisitorData>(subscribe => {
            this.eventSource.addEventListener('removeVisitor', event => {
                if(event instanceof MessageEvent) {
                    event as MessageEvent
                    const response = JSON.parse(event.data)
                    subscribe.next(response.data)
                }
            })
        })
    }

    onClearAllVisitors(): Observable<void> {
        return new Observable<void>(subscribe => {
            this.eventSource.addEventListener('clearAllVisitors', () => {
                subscribe.next()
            })
        })
    }

    onChangeVisitor(): Observable<VisitorData> {
        return new Observable<VisitorData>(subscribe => {
            this.eventSource.addEventListener('changeVisitor', event => {
                if(event instanceof MessageEvent) {
                    event as MessageEvent
                    const response = JSON.parse(event.data)
                    subscribe.next(response.data)
                }
            })
        })
    }

    close() {
        this.eventSource.close()
    }
}