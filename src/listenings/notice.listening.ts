import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { environment } from "src/environments/environment";
import { Notice } from 'src/app/page/notice/model/notice'

@Injectable()
export class NoticeListening {
    private eventSource: EventSource

    constructor(private http: HttpClient) {
        console.log('Conecting on notice sse')
        this.eventSource = new EventSource(`${environment.SERVER}/notices/sse`)
        this.eventSource.onopen = () => {
            console.log(`Conectado no notice SSE: ${this.eventSource.readyState}`)
        }
        this.eventSource.onerror = () => console.error(`Ocorreu um erro: ${this.eventSource.readyState}`)
    }

    onAddNotice(): Observable<Notice> {
        return new Observable<Notice>(subscribe => {
            this.eventSource.addEventListener('addNotice', event => {
                if(event instanceof MessageEvent) {
                    const response = JSON.parse(event.data)
                    const notice: Notice = response.data
                    subscribe.next(notice)
                }
            })
        })
    }

    onChangeNotice(): Observable<Notice> {
        return new Observable<Notice>(subscribe => {
            this.eventSource.addEventListener('changeNotice', event => {
                if(event instanceof MessageEvent) {
                    const response = JSON.parse(event.data)
                    const notice: Notice = response.data
                    subscribe.next(notice)
                }
            })
        })
    }

    onRemoveNotice(): Observable<String> {
        return new Observable<String>(subscribe => {
            this.eventSource.addEventListener('removeNotice', event => {
                if(event instanceof MessageEvent) {
                    const response = JSON.parse(event.data)
                    const noticeIdRemoved = response.data
                    subscribe.next(noticeIdRemoved)
                }
            })
        })
    }
}