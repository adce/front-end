import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisitorsComponent } from './page/visitors/visitors.component';
import { HomeComponent } from './page/home/home.component';
import { RegisterComponent } from './page/member/register/register.component';
import { NoticeComponent } from './page/notice/notice.component';
import { RegisterVisitorComponent } from './page/visitors/register-visitor/register-visitor.component';
import { RegisterNoticeComponent } from './page/notice/register-notice/register-notice.component';
import { EbdComponent } from './page/ebd/ebd.component';
import { RegisterEbdComponent } from './page/ebd/register-ebd/register-ebd.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'form', component: RegisterComponent},
  {path: 'visitantes', component: VisitorsComponent},
  {path: 'visitantes/cadastro', component: RegisterVisitorComponent},
  {path: 'notices', component: NoticeComponent},
  { path: 'notices/cadastro', component: RegisterNoticeComponent },
  { path: 'ebd', component: EbdComponent },
  { path: 'ebd/chamada', component: RegisterEbdComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
