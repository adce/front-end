import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { VisitorService } from "src/services/visitor.service";
import { Visitor, VisitorData } from "../model/visitor.model";

@Component({
    selector: 'app-register-visitor',
    templateUrl: './register-visitor.component.html',
    styleUrls: ['./register-visitor.component.scss']
})
export class RegisterVisitorComponent {
    formVisitor: FormGroup

    constructor(
        private service: VisitorService,
        private fb: FormBuilder,
        private router: Router,
    ) {
        const visitorData = this.router.getCurrentNavigation()?.extras?.state
        const visitors = visitorData?.visitors
            .map(visitor => visitor.name)
            .join('\n')
        this.formVisitor = this.fb.group({
            id: [visitorData?.id],
            isNewMember: [visitorData?.isNewMember],
            visitors: [visitors, Validators.required]
        })
    }

    save() {
        const { id, isNewMember, visitors: values } = this.formVisitor.value
        const visitors: Visitor[] = values.split('\n')
            .filter(name => name.trim())
            .map(name => ({ name: name.trim() }))
        
        const visitorData: VisitorData = { id, visitors, isNewMember };

        (visitorData.id
            ? this.service.updateVisitors(visitorData)
            : this.service.addVisitors(visitorData))
            .subscribe(() => {
                this.formVisitor.reset()
                this.formVisitor.clearValidators()
            })
    }
}