import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material";

import { ConfirmDialogComponent } from "src/app/shared/components/confirm-dialog/confirm-dialog.component";
import { VisitorService } from "src/services/visitor.service";

@Component({
    selector: 'app-confirm-clear-dialog',
    templateUrl: 'confirm-clear-dialog.component.html',
    styleUrls: ['confirm-clear-dialog.component.scss']
})
export class ConfirmClearDialogComponent {
    constructor(
        public dialogRef: MatDialogRef<ConfirmDialogComponent>,
        private service: VisitorService
    ) {
    }

    confirmRemove(): void {
        this.service.removeAllVistors()
            .subscribe(() => this.dialogRef.close())
    }
}