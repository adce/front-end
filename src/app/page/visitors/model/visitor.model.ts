export interface Visitor {
    name: String
}

export interface VisitorData {
    id?: String
    isNewMember?: Boolean
    visitors: Visitor[]
}