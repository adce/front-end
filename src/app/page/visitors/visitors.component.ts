import { ChangeDetectorRef, Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { VisitorData } from './model/visitor.model'
import { VisitorService } from "src/services/visitor.service";
import { VisitorListening } from 'src/listenings/visitor.listening'
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ConfirmClearDialogComponent } from "./components/confirm-clear-dialog/confirm-clear-dialog.component";

@Component({
  selector: 'app-visitors',
  host: {style: 'flex: 1 1 auto'},
  templateUrl: './visitors.component.html',
  styleUrls: ['./visitors.component.scss'],
  providers: [VisitorListening]
})
export class VisitorsComponent implements OnInit, OnDestroy {
  visitors: VisitorData[]
  newMembers: VisitorData[]
  private eventSource: EventSource
  
  constructor(
    private ref: ChangeDetectorRef,
    private dialog: MatDialog,
    private router: Router,
    private service: VisitorService,
    private visitorListening: VisitorListening
    ) {
    this.visitors = []
    this.newMembers = []
  }

  ngOnInit() {
    this.service.findAll()
      .subscribe(visitors => visitors.forEach(visitor => this.addValue(visitor)))

    this.visitorListening
      .onAddVisitor()
      .subscribe(visitor => {
        this.addValue(visitor)
        this.ref.detectChanges()
      })
    
    this.visitorListening
      .onRemoveVisitor()
      .subscribe(visitorData => {
        if(visitorData.isNewMember) {
          this.newMembers = this.newMembers.filter(visitor => visitor.id !== visitorData.id)
        } else {
          this.visitors = this.visitors.filter(visitor => visitor.id !== visitorData.id)
        }
        this.ref.detectChanges()
      })

      this.visitorListening
        .onClearAllVisitors()
        .subscribe(() => {
          this.visitors = []
          this.newMembers = []
          this.ref.detectChanges()
        })

      this.visitorListening.onChangeVisitor()
        .subscribe(visitorData => {
          if(visitorData.isNewMember) {
            const index = this.newMembers.findIndex(visitor => visitor.id === visitorData.id)
            if(index < 0) {
              this.newMembers.push(visitorData)
              this.visitors = this.visitors.filter(visitor => visitor.id !== visitorData.id)
            } else {
              this.newMembers[index] = visitorData
            }
          } else {
            const index = this.visitors.findIndex(visitor => visitor.id === visitorData.id)
            if(index < 0) {
              this.visitors.push(visitorData)
              this.newMembers = this.newMembers.filter(visitor => visitor.id !== visitorData.id)
            } else {
              this.visitors[index] = visitorData
            }
          }
        })
  }
  
  ngOnDestroy() {
    this.visitorListening.close()
  }

  addValue(visitor: VisitorData) {
    visitor.isNewMember
      ? this.newMembers?.push(visitor)
      : this.visitors?.push(visitor)
  }

  editVisitor(visitor: VisitorData) {
    this.router.navigateByUrl('visitantes/cadastro', {state: visitor})
  }

  amountVisitors() {
    const qtdNewMembers = this.newMembers.reduce((size, members) => members.visitors.length + size, 0)
    const qtdVisitors = this.visitors.reduce((size, visitor) => visitor.visitors.length + size, 0)
    return qtdNewMembers + qtdVisitors
  }

  confirmRemove(visitorData: VisitorData) {
    this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: visitorData
    })
  }

  confirmClear() {
    this.dialog.open(ConfirmClearDialogComponent, {
      width: '250px',
    })
  }
}
