export default interface RegisterPontuationRequest {
    studentId: number
    isPresent: boolean
    hasBible: boolean
    hasMagazine: boolean
    hasFood: boolean
    hasVisitor: boolean
    hasOffer: boolean
}