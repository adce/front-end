export default interface ClassResponse {
    id: number
    name: string
}