export default interface StudentResponse {
    id: number
    name: string
    createdAt: number
    isPresent: boolean
    hasOffer: boolean
    hasMagazine: boolean
    hasBible: boolean
    hasFood: boolean
    hasVisitor: boolean
}