import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EbdComponent } from './ebd.component';

describe('EbdComponent', () => {
  let component: EbdComponent;
  let fixture: ComponentFixture<EbdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EbdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EbdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
