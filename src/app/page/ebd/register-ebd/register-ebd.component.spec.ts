import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterEbdComponent } from './register-ebd.component';

describe('RegisterEbdComponent', () => {
  let component: RegisterEbdComponent;
  let fixture: ComponentFixture<RegisterEbdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterEbdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterEbdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
