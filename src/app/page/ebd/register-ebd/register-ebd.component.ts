import { Component, OnInit } from '@angular/core';
import StudentResponse from '../models/responses/student.response';
import { EbdService } from 'src/services/ebd.service';
import ClassResponse from '../models/responses/class.response';
import RegisterPontuationRequest from '../models/requests/register-pontuation.request';
import { scheduled } from 'rxjs';

@Component({
  selector: 'app-register-ebd',
  templateUrl: './register-ebd.component.html',
  styleUrls: ['./register-ebd.component.scss']
})
export class RegisterEbdComponent implements OnInit {
  displayedColumns: string[] = ['name', 'check'];
  classes: ClassResponse[]
  students: StudentResponse[]
  classSelected: number
  date = Date.now()
  hasRegister = false

  constructor(private ebdService: EbdService) { }

  ngOnInit(): void {
    this.ebdService.listClasses()
      .subscribe(classes => {
        this.classes = classes
        this.classSelected = classes[0].id
        this.ebdService.findStudentsbyClass(this.classSelected)
          .subscribe(students => {
            this.students = students
            this.hasRegister = students.some(student => !!student.createdAt)
          })
      })
  }

  selectClass(classId: number) {
    this.ebdService.findStudentsbyClass(classId)
      .subscribe(students => {
        this.students = students
        this.hasRegister = students.some(student => !!student.createdAt)
      })
  }

  register() {
    const request: RegisterPontuationRequest[] = this.students.map(student => ({
      studentId: student.id,
      isPresent: student.isPresent,
      hasBible: student.hasBible,
      hasFood: student.hasFood,
      hasMagazine: student.hasMagazine,
      hasOffer: student.hasOffer,
      hasVisitor: student.hasVisitor
    }))
    this.ebdService.registerPontuation(this.classSelected, request)
      .subscribe(response => {
        console.log(response)
        this.hasRegister = true
      })
  }
}
