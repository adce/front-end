import { Component, OnInit } from '@angular/core';
import PontuationStudentResponse from './models/responses/pontuation-student.response';
import PontuationClassResponse from './models/responses/pontuation-class.response';
import { EbdService } from 'src/services/ebd.service';
import ClassResponse from './models/responses/class.response';

@Component({
  selector: 'app-ebd',
  templateUrl: './ebd.component.html',
  styleUrls: ['./ebd.component.scss']
})
export class EbdComponent implements OnInit {
  displayedColumns: string[] = ['name', 'pontuation'];
  classesPontuation: PontuationClassResponse[]
  students: PontuationStudentResponse[]
  classes: ClassResponse[] = []
  classSelected!: number

  constructor(private ebdService: EbdService) {
  }

  ngOnInit(): void {
    this.ebdService.listClasses()
      .subscribe(classes => {
        this.classes = classes
        this.classSelected = classes[0].id
        this.ebdService.findStudentsPontuationByClass(this.classSelected)
          .subscribe(studentsPontuation => this.students = studentsPontuation)
      })
    
    this.ebdService.findClassesPontuation()
      .subscribe(classesPontuation => this.classesPontuation = classesPontuation)
  }

  selectClass(classId: number) {
    this.ebdService.findStudentsPontuationByClass(classId)
      .subscribe(studentsPontuation => this.students = studentsPontuation)
  }
}
