import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  options = [
    {
      name: 'Avisos',
      route: 'notices',
      icon: 'assets/icons/png/noticeboard.png'
    },
    {
      name: 'Visitantes',
      route: '/visitantes',
      icon: 'assets/icons/png/visitor.png'
    },
    {
      name: 'E.B.D',
      route: '/ebd',
      icon: 'assets/icons/png/bible.png'
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
