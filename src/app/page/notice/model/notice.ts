export interface Notice {
    id?: String,
    department: String,
    notice: String
}