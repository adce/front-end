import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

import { Notice } from "../../model/notice";
import { NoticeService } from "src/services/notice.service";
import { ConfirmDialogComponent } from "src/app/shared/components/confirm-dialog/confirm-dialog.component";

@Component({
    selector: 'app-remove-notice-dialog',
    templateUrl: './remove-notice-dialog.component.html',
    styleUrls: ['./remove-notice-dialog.component.scss']
})
export class RemoveNoticeDialogComponent {
    constructor(
        public dialogRef: MatDialogRef<ConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public notice: Notice,
        private service: NoticeService
    ) {}

    confirmRemove() {
        this.service.removeNotice(this.notice.id)
        .subscribe(() => this.dialogRef.close())
    }
}