import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';

import { NoticeListening } from 'src/listenings/notice.listening';
import { Notice } from './model/notice';
import { NoticeService } from 'src/services/notice.service';
import { RemoveNoticeDialogComponent } from './componentts/remove-notice-dialog/remove-notice-dialog.component';

@Component({
  selector: 'app-notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.scss'],
  providers: [NoticeListening]
})
export class NoticeComponent implements OnInit {

  notices: Notice[] = []

  constructor(
    private ref: ChangeDetectorRef,
    private router: Router,
    private dialog: MatDialog,
    private listening: NoticeListening,
    private service: NoticeService
  ) { }

  ngOnInit(): void {
    this.service.findAll()
      .subscribe(notices => this.notices = notices)

    this.listening.onAddNotice()
      .subscribe(notice => {
        this.notices.push(notice)
        this.ref.detectChanges()
      })

      this.listening.onChangeNotice()
        .subscribe(notice => {
          const noticeIndex = this.notices.findIndex(notice => notice.id === notice.id)
          this.notices[noticeIndex] = notice
          this.ref.detectChanges()
        })

      this.listening.onRemoveNotice()
        .subscribe(noticeIdRemoved => {
          this.notices = this.notices.filter(notice => notice.id !== noticeIdRemoved)
          this.ref.detectChanges()
        })
  }

  editNotice(notice: Notice) {
    this.router.navigateByUrl('notices/cadastro', {state: notice})
  }

  confirmRemove(notice: Notice) {
    this.dialog.open(RemoveNoticeDialogComponent, {
      width: '250px',
      data: notice
    })
  }

}
