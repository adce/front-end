import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NoticeService } from "src/services/notice.service";

@Component({
    selector: 'app-register-notice',
    templateUrl: './register-notice.component.html',
    styleUrls: ['./register-notice.component.scss']
})
export class RegisterNoticeComponent implements OnInit {
    formNotice: FormGroup

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private service: NoticeService
    ) {
        const notice = this.router.getCurrentNavigation()?.extras?.state
        this.formNotice = this.fb.group({
            id: [notice?.id],
            department: [notice?.department, Validators.required],
            notice: [notice?.notice, [Validators.required, Validators.min(5)]]
        })
    }

    ngOnInit(): void {
    }

    save() {
        (this.formNotice.value.id
            ? this.service.updateNotice(this.formNotice.value)
            : this.service.addNotice(this.formNotice.value))
            .subscribe(response => {
                console.log(response)
                this.formNotice.reset()
            })
    }
} 