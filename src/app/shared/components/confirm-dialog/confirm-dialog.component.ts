import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { VisitorData } from "src/app/page/visitors/model/visitor.model";
import { VisitorService } from "src/services/visitor.service";

@Component({
    selector: 'app-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent {
    constructor(
        public dialogRef: MatDialogRef<ConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public visitorData: VisitorData,
        private service: VisitorService
    ) {
    }

    confirmRemove(): void {
        this.service.removeVisitor(this.visitorData.id)
            .subscribe(() => this.dialogRef.close())
    }
}