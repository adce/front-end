import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  menus = ['Visitante', 'Avisos', 'E.B.D'].sort()

  constructor() { }

  ngOnInit(): void {
  }

}
