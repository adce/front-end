import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatRippleModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HeaderComponent } from './core/header/header.component';
import { VisitorsComponent } from './page/visitors/visitors.component';
import { RegisterVisitorComponent } from './page/visitors/register-visitor/register-visitor.component';
import { EbdComponent } from './page/ebd/ebd.component';
import { RegisterEbdComponent } from './page/ebd/register-ebd/register-ebd.component';

import { GraphQLModule } from './graphql.module';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';


import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';

import { environment } from '../environments/environment';
import { HomeComponent } from './page/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './core/menu/menu.component';
import { RegisterComponent } from './page/member/register/register.component';
import { NoticeComponent } from './page/notice/notice.component';
import { RegisterNoticeComponent } from './page/notice/register-notice/register-notice.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ConfirmClearDialogComponent } from './page/visitors/components/confirm-clear-dialog/confirm-clear-dialog.component';
import { RemoveNoticeDialogComponent } from './page/notice/componentts/remove-notice-dialog/remove-notice-dialog.component';

import { VisitorService } from 'src/services/visitor.service';
import { NoticeService } from 'src/services/notice.service'

registerLocaleData(ptBr)

const config: SocketIoConfig = { url: environment.SERVER, options: {path: '/wsapp'} };
console.log(environment)
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    VisitorsComponent,
    RegisterVisitorComponent,
    HomeComponent,
    MenuComponent,
    RegisterComponent,
    NoticeComponent,
    RegisterNoticeComponent,
    ConfirmDialogComponent,
    ConfirmClearDialogComponent,
    RemoveNoticeDialogComponent,
    EbdComponent,
    RegisterEbdComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    // Angular Material
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatInputModule,
    MatStepperModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    MatRippleModule,
    MatCardModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTableModule,
    MatDividerModule,

    GraphQLModule,
    SocketIoModule.forRoot(config),
    BrowserAnimationsModule
  ],
  entryComponents: [
  ],
  providers: [
    HttpClient,
    {
      provide: LOCALE_ID,
      useValue: 'pt-PT'
    },
    VisitorService,
    NoticeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
