import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Notice } from "src/app/page/notice/model/notice";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class NoticeService {
    constructor(private http: HttpClient) {}

    findAll(): Observable<Notice[]> {
        return this.http.get<Notice[]>(`${environment.SERVER}/notices`)
    }

    addNotice(notice: Notice): Observable<Notice> {
        return this.http.post<Notice>(`${environment.SERVER}/notices`, notice)
    }

    updateNotice(notice: Notice): Observable<Notice> {
        return  this.http.put<Notice>(`${environment.SERVER}/notices/${notice.id}`, notice)
    }

    removeNotice(noticeId: String): Observable<void> {
        return this.http.delete<void>(`${environment.SERVER}/notices/${noticeId}`)
    }
}