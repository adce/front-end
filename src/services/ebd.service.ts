import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import RegisterPontuationRequest from "src/app/page/ebd/models/requests/register-pontuation.request";
import ClassResponse from "src/app/page/ebd/models/responses/class.response";
import PontuationClassResponse from "src/app/page/ebd/models/responses/pontuation-class.response";
import PontuationStudentResponse from "src/app/page/ebd/models/responses/pontuation-student.response";
import StudentResponse from "src/app/page/ebd/models/responses/student.response";

import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class EbdService {
    constructor(private http: HttpClient) {}

    listClasses(): Observable<ClassResponse[]> {
        return this.http.get<ClassResponse[]>(`${environment.SERVER}/ebd/classes`)
    }

    findClassesPontuation(): Observable<PontuationClassResponse[]> {
        return this.http.get<PontuationClassResponse[]>(`${environment.SERVER}/ebd/classes/pontuation`)
    }

    findStudentsbyClass(classId: number): Observable<StudentResponse[]> {
        return this.http.get<StudentResponse[]>(`${environment.SERVER}/ebd/classes/${classId}/students`)
    }

    findStudentsPontuationByClass(classId: number): Observable<PontuationStudentResponse[]> {
        return this.http.get<PontuationStudentResponse[]>(`${environment.SERVER}/ebd/classes/${classId}/students/pontuation`)
    }

    registerPontuation(classId: number, request: RegisterPontuationRequest[]) {
        return this.http.post<any>(`${environment.SERVER}/ebd/classes/${classId}/pontuation`, request)
    }
}