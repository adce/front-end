import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { VisitorData } from "src/app/page/visitors/model/visitor.model";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root',
})
export class VisitorService {
    constructor(private http: HttpClient) {}

    addVisitors(visitors: VisitorData): Observable<void> {
        return this.http.post<void>(`${environment.SERVER}/visitor`, visitors)
    }

    updateVisitors(visitorData: VisitorData): Observable<void> {
        return this.http.put<void>(`${environment.SERVER}/visitor/${visitorData.id}`, visitorData)
    }

    findAll(): Observable<VisitorData[]> {
        return this.http.get<VisitorData[]>(`${environment.SERVER}/visitor`)
    }

    removeVisitor(id: String): Observable<void> {
        return this.http.delete<void>(`${environment.SERVER}/visitor/${id}`)
    }

    removeAllVistors(): Observable<void> {
        return this.http.delete<void>(`${environment.SERVER}/visitor`)
    }
}